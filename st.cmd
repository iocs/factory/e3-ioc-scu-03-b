
require stream
require scu
require essioc


epicsEnvSet("STREAM_PROTOCOL_PATH","${scu_DB}")

epicsEnvSet("SCU",     "SCU06")
epicsEnvSet("BRD_A",     "A")
epicsEnvSet("BRD_B",     "B")
epicsEnvSet("IP_ADDR_A", "172.16.44.23")
epicsEnvSet("IP_ADDR_B", "172.16.44.24")
epicsEnvSet("PIPELINE_ID","201631")

epicsEnvSet("MC_TYPE1", "rs485mc")
epicsEnvSet("MC_TYPE2", "rs485mc")
epicsEnvSet("MC_TYPE3", "rs485mc")
epicsEnvSet("MC_TYPE4", "rs485mc")
epicsEnvSet("MC_TYPE5", "rs485mc")
epicsEnvSet("MC_TYPE6", "rs485mc")
epicsEnvSet("MC_TYPE7", "rs485mc")
epicsEnvSet("MC_TYPE8", "dymc")
epicsEnvSet("MC_TYPE9", "rs485mc")
epicsEnvSet("MC_TYPE10", "rs485mc")
epicsEnvSet("MC_TYPE11", "dymc")
epicsEnvSet("MC_TYPE12", "dymc")


epicsEnvSet("PORT_A", "FBIS-$(SCU)-$(BRD_A)")
epicsEnvSet("PORT_B", "FBIS-$(SCU)-$(BRD_B)")
epicsEnvSet("P", "FBIS-$(SCU):")

drvAsynIPPortConfigure("$(PORT_A)",     "$(IP_ADDR_A):8080", 0,0,1)
drvAsynIPPortConfigure("$(PORT_B)",     "$(IP_ADDR_B):8080", 0,0,1)

iocshLoad("$(scu_DIR)SCU.iocsh", "PORT=$(PORT_A), P=$(P), R=Ctrl-Ser-01:, BRD=$(BRD_A)")
iocshLoad("$(scu_DIR)SCU.iocsh", "PORT=$(PORT_B), P=$(P), R=Ctrl-Ser-02:, BRD=$(BRD_B)")

iocshLoad("$(scu_DIR)SCU_man.iocsh", "PORT=$(PORT_A), PORT_A=$(PORT_A), PORT_B=$(PORT_B), P=$(P)")

iocshLoad("$(essioc_DIR)/common_config.iocsh")

callbackSetQueueSize(5000)
scanOnceSetQueueSize(5000)

iocInit

